using UnityEditor;
using System.IO;

namespace CodeBase.AssetBandleLogic
{
    public class CreateAssetBundles
    {
        [MenuItem("Bundles/Build AssetBundles")]
        static void BuildAllAssetBundles()
        {
            string assetBundleDirectory = "Assets/AssetBundles";
            if (!Directory.Exists(assetBundleDirectory))
            {
                Directory.CreateDirectory(assetBundleDirectory);
            }
            BuildPipeline.BuildAssetBundles(assetBundleDirectory,
                                            BuildAssetBundleOptions.None,
                                            BuildTarget.Android);
        }
    }
}
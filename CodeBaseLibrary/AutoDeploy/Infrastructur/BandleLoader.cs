﻿using AutoDeploy.Logic;
using System;
using System.Reflection;
using UnityEngine;

namespace AutoDeploy.Infrastructur
{
    public class BandleLoader
    {
        #region Structs

        public struct ScriptsArg
        {
            public string path;
            public string nameInitClass;
        }

        #endregion

        #region Private Fields

        private ICoroutineRunner _coroutineRunner;

        #endregion

        #region Constructs
        public BandleLoader(ICoroutineRunner coroutineRunner)
        {
            _coroutineRunner = coroutineRunner;
        }

        #endregion

        #region Public Methods

        public void LoadSceneFromBundle(string path, Action<string> callback)
        {
            string sceneName = null;
            _coroutineRunner.StartCoroutine(Api.GetAssetBundleRequest(path, (code, response) =>
            {
                if (code == 200)
                {
                    if (response.isStreamedSceneAssetBundle)
                    {
                        string[] scenePaths = response.GetAllScenePaths();
                        sceneName = System.IO.Path.GetFileNameWithoutExtension(scenePaths[0]);
                    }
                }
                else
                {
                    Debug.LogError(code.ToString() + " " + response);
                }
                callback(sceneName);
            }));
        }

        public void LoadPrefabsFromBundle(string path, Action<AssetBundle> callback)
        {
            _coroutineRunner.StartCoroutine(Api.GetAssetBundleRequest(path, (code, response) =>
                {
                    if (code == 200)
                    {
                        GameBootstrapper.Instance.StartLevelInformant.SetLevelPrefabsBundle(response);
                    }
                    else
                    {
                        Debug.LogError(code.ToString() + " " + response);
                        GameBootstrapper.Instance.StartLevelInformant.SetLevelPrefabsBundle(null);
                    }
                    callback(response);
                }));
        }
        public void LoadScriptsFromBundle(string path, Action<Type> callback, string nameInitClass = "LevelBootstrapper")
        {
            Type type = null;
            _coroutineRunner.StartCoroutine(Api.GetAssetBundleRequest(path, (code, response) =>
            {
                var names = response.GetAllAssetNames();
                foreach (var name in names)
                {
                    Debug.Log(name);
                }

                string[] namesInBundle = response.GetAllAssetNames();
                TextAsset scriptsEnvironment = response.LoadAsset(namesInBundle[0]) as TextAsset;
                Assembly assembly = Assembly.Load(scriptsEnvironment.bytes);
                type = assembly.GetType(nameInitClass);

                callback(type);
            }));

            /*
             * script for load from addressable
             * 
            AsyncOperationHandle<TextAsset> handle = assetReference.LoadAssetAsync<TextAsset>();
            await handle.Task;
            if(handle.Status == AsyncOperationStatus.Succeeded)
            {
                var assembly = Assembly.Load(handle.Result.bytes);
                var type = assembly.GetType("BootstrapLevel");
 
                Debug.Log(type);

                GameObject bootsLvl = new GameObject();
                bootsLvl.AddComponent(type);
                //Addressables.Release(handle);
            }
            */
        }
        #endregion
    }
}
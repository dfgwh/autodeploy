using AutoDeploy.Infrastructur.StateMachine.States;
using AutoDeploy.Logic;
using System.Linq;
using UnityEngine;

namespace AutoDeploy.Infrastructur
{
    /// <summary>
    /// ����� ������ ������ ������
    /// ����� ���������� ������������� "�������" ������
    /// </summary>
    public class GameBootstrapper : MonoBehaviour, ICoroutineRunner
    {
        #region Properties

        public static GameBootstrapper Instance { get => instance; set => instance = value; }
        public Game Game { get => _game; set => _game = value; }
        public string MainUrlPath { get => _mainUrlPath; }
        public ILevelInformant StartLevelInformant { get => _startLevelInformant; set => _startLevelInformant = value; }

        #endregion

        #region Unity Editor

        [SerializeField] private string _mainUrlPath;
        [SerializeField] private LoadingCurtain _loadingCurtain;

        #endregion

        #region Private Fields

        private Game _game;
        private ILevelInformant _startLevelInformant;
        private static GameBootstrapper instance = null;

        #endregion

        #region Unity Methods

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                DestroyImmediate(gameObject);
            }
        }

        private void Start()
        {
            FindInformant();

            Game = new Game(this, _loadingCurtain);
            Game.StateMachine.Enter<BootstrapState>();
        }

        #endregion

        #region Private Methods

        private void FindInformant()
        {
            //TODO �������� ����� ����������� ����, linq �� ��������� ����������� �� ����� ����������� �����
            var ss = FindObjectsOfType<MonoBehaviour>().OfType<ILevelInformant>();
            _startLevelInformant = ss.FirstOrDefault();
        }

        #endregion

    }
}
﻿using System.Collections;
using UnityEngine;

namespace AutoDeploy.Infrastructur
{
    public interface ICoroutineRunner
    {
        Coroutine StartCoroutine(IEnumerator coroutine);
    }
}
﻿using UnityEngine;

namespace AutoDeploy.Infrastructur
{
    public interface ILevelInformant
    {
        /// <summary>
        /// Получить ссылку на загрузку сцены уровня
        /// </summary>
        /// <returns>строка с адресом</returns>
        string GetLevelSceneUrl();

        /// <summary>
        /// Получить ссылку на загрузку скриптов уровня
        /// </summary>
        /// <returns>строка с адресом</returns>
        string GetLevelScriptsUrl();

        /// <summary>
        /// Получить ссылку на загрузку префабов уровня
        /// </summary>
        /// <returns>строка с адресом</returns>
        string GetLevelPrefabsUrl();

        /// <summary>
        /// Получить имя инициализируемого класса
        /// Класс должен быть унаследован от MonoBehaviour и 
        /// в методе Start() инициализировать остальную часть кода
        /// </summary>
        /// <returns>имя класса</returns>
        string GetInitScriptName();

        /// <summary>
        /// Сохранить ссылку на загруженный бандл с префабами
        /// </summary>
        /// <param name="bundle">бандл с префабами</param>
        void SetLevelPrefabsBundle(AssetBundle bundle);

        /// <summary>
        /// Получить ссылку на бандл с префабами
        /// </summary>
        /// <returns>бандл с префабами</returns>
        AssetBundle GetLevelPrefabsBundle();

        /// <summary>
        /// Скачать стартовую информацию и передать обратным вызовом ссылку на загрузку 
        /// сцены и ссылку на загрузку скриптов стартового уровня
        /// </summary>
        /// <param name="callback">(scene url, scripts url)</param>
        void DownloadStartLevelInfo(System.Action<string, string> callback);
    }
}
﻿namespace AutoDeploy.Infrastructur.StateMachine
{
    public interface IPayloadedState<TPayload> : IExtableState
    {
        void Enter(TPayload payload);
    }

}
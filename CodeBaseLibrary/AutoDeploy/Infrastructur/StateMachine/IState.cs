﻿namespace AutoDeploy.Infrastructur.StateMachine
{
    public interface IState : IExtableState
    {
        void Enter();
    }
}
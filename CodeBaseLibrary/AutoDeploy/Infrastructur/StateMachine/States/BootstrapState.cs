﻿using AutoDeploy.Logic;
using UnityEngine;

namespace AutoDeploy.Infrastructur.StateMachine.States
{
    public class BootstrapState : IState
    {
        #region Private Fields

        private readonly GameStateMachine _stateMachine;
        private readonly SceneLoader _sceneLoader;

        #endregion

        #region Constructors

        public BootstrapState(GameStateMachine stateMachine, SceneLoader sceneLoader)
        {
            _stateMachine = stateMachine;
            _sceneLoader = sceneLoader;
        }

        #endregion

        #region Public Methods

        public void Enter()
        {
            _stateMachine.LoadingCurtain.Show(LoadingCurtain.LoadingMode.withoutProgress);

            RegisterServices();

            EnterLoadLevel();
        }

        public void Exit()
        {
            //_stateMachine.LoadingCurtain.Hide();
        }

        #endregion

        #region Private Methods

        private void EnterLoadLevel()
        {
            GameBootstrapper.Instance.StartLevelInformant.DownloadStartLevelInfo(
                (sceneUrl, scriptsUrl) =>
                {
                    Auto_Deploy.LoadScene(sceneUrl);
                });
        }

        private void RegisterServices()
        {
            RegisterWebRequestsService();
        }

        private static void RegisterWebRequestsService()
        {
            Api.MainUrlPath = GameBootstrapper.Instance.MainUrlPath;
            if (Api.MainUrlPath == "")
            {
                Debug.LogWarning("main url path is empty");
            }
        }

        #endregion

    }
}

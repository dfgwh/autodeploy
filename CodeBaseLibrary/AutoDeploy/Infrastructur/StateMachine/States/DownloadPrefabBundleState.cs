﻿using AutoDeploy.Logic;
using UnityEngine;

namespace AutoDeploy.Infrastructur.StateMachine.States
{
    public class DownloadPrefabBundleState : IPayloadedState<string>
    {
        #region Constants

        private const string LOADING_TEXT = "Download Prefabs ...";

        #endregion

        #region Private Fields

        private GameStateMachine _stateMachine;
        private BandleLoader _bandleLoader;

        #endregion

        #region Constructors

        public DownloadPrefabBundleState(GameStateMachine gameStateMachine, BandleLoader bandleLoader)
        {
            _stateMachine = gameStateMachine;
            _bandleLoader = bandleLoader;
        }

        #endregion

        #region Interface Methods

        public void Enter(string payload)
        {
            _stateMachine.LoadingCurtain.Show(LoadingCurtain.LoadingMode.withoutProgress, LOADING_TEXT);

            if (payload == "")
            {
                Debug.Log("<color=red>url to load prefab is missing</color>");
                LoadScriptsEnvironment();
            }
            else
            {
                _bandleLoader.LoadPrefabsFromBundle(payload, (response) =>
                {
                    LoadScriptsEnvironment();
                });
            }
        }

        public void Exit()
        {
            //_stateMachine.LoadingCurtain.Hide();
        }

        #endregion

        #region Private Methods

        private void LoadScriptsEnvironment()
        {
            Auto_Deploy.LoadScriptsEnvironment(
                GameBootstrapper.Instance.StartLevelInformant.GetLevelScriptsUrl(),
                GameBootstrapper.Instance.StartLevelInformant.GetInitScriptName());
        }

        #endregion
    }
}
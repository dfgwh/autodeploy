﻿using AutoDeploy.Logic;
using UnityEngine;

namespace AutoDeploy.Infrastructur.StateMachine.States
{
    public class DownloadSceneBundleState : IPayloadedState<string>
    {
        #region Constants

        private const string LOADING_TEXT = "Download Scene ...";

        #endregion

        #region Private Fields

        private GameStateMachine _stateMachine;
        private BandleLoader _bandleLoader;

        #endregion

        #region Constructors

        public DownloadSceneBundleState(GameStateMachine gameStateMachine, BandleLoader bandleLoader)
        {
            _stateMachine = gameStateMachine;
            _bandleLoader = bandleLoader;
        }

        #endregion

        #region Interface Methods

        public void Enter(string path)
        {
            _stateMachine.LoadingCurtain.Show(LoadingCurtain.LoadingMode.withoutProgress, LOADING_TEXT);

            if (path == "")
            {
                Debug.Log("<color=red>url to scene bundle is missing</color>");
                _stateMachine.Enter<DownloadPrefabBundleState, string>(GameBootstrapper.Instance.StartLevelInformant.GetLevelPrefabsUrl());
            }
            else
            {
                _bandleLoader.LoadSceneFromBundle(path, (sceneName) =>
                {
                    if (sceneName != null)
                    {
                        _stateMachine.Enter<LoadLevelState, string>(sceneName);
                    }
                    else
                    {
                        Debug.Log("<color=red>scene in bundle is missing</color>");
                        _stateMachine.Enter<DownloadPrefabBundleState, string>(GameBootstrapper.Instance.StartLevelInformant.GetLevelPrefabsUrl());
                    }
                });
            }
        }

        public void Exit()
        {
            //_stateMachine.LoadingCurtain.Hide();
        }

        #endregion
    }
}
﻿using AutoDeploy.Logic;
using UnityEngine;

namespace AutoDeploy.Infrastructur.StateMachine.States
{
    public class DownloadScriptsBundleState : IPayloadedState<BandleLoader.ScriptsArg>
    {
        #region Constants

        private const string LOADING_TEXT = "Loading scripts ...";

        #endregion

        #region Private Fields

        private GameStateMachine _stateMachine;
        private BandleLoader _bandleLoader;

        #endregion

        #region Constructors

        public DownloadScriptsBundleState(GameStateMachine gameStateMachine, BandleLoader bandleLoader)
        {
            _stateMachine = gameStateMachine;
            _bandleLoader = bandleLoader;
        }

        #endregion

        #region Interface Methods

        public void Enter(BandleLoader.ScriptsArg payload)
        {
            string bundleUrl = payload.path;
            string nameInitClass = payload.nameInitClass;

            _stateMachine.LoadingCurtain.Show(LoadingCurtain.LoadingMode.withoutProgress, LOADING_TEXT);

            if (bundleUrl == "")
            {
                Debug.Log("<color=red>url to load scripts is missing</color>");
                _stateMachine.Enter<GameLoopState>();
            }
            else if (nameInitClass == "")
            {
                Debug.Log("<color=red>nameScriptInit to start loading scripts is missing</color>");
                _stateMachine.Enter<GameLoopState>();
            }
            else
            {
                _bandleLoader.LoadScriptsFromBundle(bundleUrl, (type) =>
                {
                    if (type == null)
                    {
                        Debug.LogError(nameInitClass + ".cs is missing in bundle");
                    }
                    else
                    {
                        GameObject bootsLvl = new GameObject();
                        bootsLvl.AddComponent(type);
                    }

                    _stateMachine.Enter<GameLoopState>();

                }, nameInitClass);
            }
        }

        public void Exit()
        {
            _stateMachine.LoadingCurtain.Hide();
        }

        #endregion
    }
}
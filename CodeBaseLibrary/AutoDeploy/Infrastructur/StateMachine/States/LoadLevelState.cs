﻿using AutoDeploy.Logic;

namespace AutoDeploy.Infrastructur.StateMachine.States
{
    internal class LoadLevelState : IPayloadedState<string>
    {
        #region Constants

        private const string LOADING_TEXT = "Loading scene ...";

        #endregion

        #region Private Fields

        private GameStateMachine _stateMachine;
        private SceneLoader _sceneLoader;

        #endregion

        #region Constructors

        public LoadLevelState(GameStateMachine gameStateMachine, SceneLoader sceneLoader)
        {
            _stateMachine = gameStateMachine;
            _sceneLoader = sceneLoader;
        }

        #endregion

        #region Interface MEthods

        public void Enter(string sceneName)
        {
            _stateMachine.LoadingCurtain.Show(LoadingCurtain.LoadingMode.withProgress, LOADING_TEXT);
            _sceneLoader.Load(sceneName, _stateMachine.LoadingCurtain, OnLoaded);
        }

        public void Exit()
        {
            _stateMachine.LoadingCurtain.Hide();
        }

        #endregion

        #region Private Methods

        private void OnLoaded()
        {
            _stateMachine.Enter<DownloadPrefabBundleState, string>(GameBootstrapper.Instance.StartLevelInformant.GetLevelPrefabsUrl());
        }

        #endregion
    }
}
﻿using AutoDeploy.Infrastructur;
using AutoDeploy.Infrastructur.StateMachine.States;

namespace AutoDeploy.Logic
{
    public class Auto_Deploy
    {
        #region Public Static Methods

        public static void LoadScene(string bundleUrl)
        {
            GameBootstrapper.Instance.Game.StateMachine.Enter<DownloadSceneBundleState, string>(bundleUrl);
        }

        public static void LoadScriptsEnvironment(string bundleUrl, string nameInitClass = null)
        {
            BandleLoader.ScriptsArg arg;
            arg.path = bundleUrl;
            arg.nameInitClass = nameInitClass;
            GameBootstrapper.Instance.Game.StateMachine.Enter<DownloadScriptsBundleState, BandleLoader.ScriptsArg>(arg);
        }

        #endregion
    }
}
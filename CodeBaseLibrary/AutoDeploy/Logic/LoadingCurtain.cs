﻿using UnityEngine;
using UnityEngine.UI;

namespace AutoDeploy.Logic
{
    public class LoadingCurtain : PageUI
    {

        #region Enums

        public enum LoadingMode
        {
            withProgress,
            withoutProgress,
        }

        #endregion

        #region Constants

        private const string DEFAULT_LOADING_TEXT = "Loading";

        #endregion

        #region Unity Editor

        [SerializeField] private Slider _progressSlider;
        [SerializeField] private GameObject _waitObject;
        [SerializeField] private Text _loadingText;

        #endregion

        #region Unity Methods

        private void Awake()
        {
            DontDestroyOnLoad(gameObject);
            _progressSlider.interactable = false;
        }

        #endregion

        #region Overrides Methods

        public override void Show()
        {
            Show(LoadingMode.withoutProgress);
        }
        public override void Hide()
        {
            base.Hide();
            _progressSlider.value = 0;
        }

        #endregion

        #region Publc Methods

        public void SetProgressValue(float value)
        {
            _progressSlider.value = value;
        }

        public void Show(LoadingMode loadingMode, string loadingText = DEFAULT_LOADING_TEXT)
        {
            base.Show();

            _loadingText.text = loadingText;

            if (loadingMode == LoadingMode.withoutProgress)
            {
                _waitObject.SetActive(true);
                _progressSlider.gameObject.SetActive(false);
            }
            else
            {
                _waitObject.SetActive(false);
                _progressSlider.gameObject.SetActive(true);
            }
        }

        #endregion
    }
}

﻿using UnityEngine;

class BootstrapLevel : MonoBehaviour
{
    private void Start()
    {
        Init();
    }

    public void Init()
    {
        Debug.Log("BootstrapLevel: <color=green>init level scripts environment</color>");

        var UIGameObject = new GameObject("LevelUI");
        UIGameObject.AddComponent<InitLevelUI>();

        GameObject logo3d = GameObject.Find("Logo3d");
        logo3d.AddComponent<ObjectRotate_Level1>();
    }
}

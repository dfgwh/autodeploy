using UnityEngine;

public class ObjectRotate_Level1 : MonoBehaviour
{
    #region Private Fields

    [SerializeField] private float rotateSpeed = 1f;
    private Vector3 transformVector;

    #endregion

    #region Unity Methods

    private void Update()
    {
        transformVector = Vector3.forward * transform.rotation.x + Vector3.right * transform.rotation.z + Vector3.up * rotateSpeed * Time.deltaTime;
        transform.Rotate(transformVector);
    }

    #endregion
}
﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class BootstrapLevel : MonoBehaviour
{
    private void Start()
    {
        Init();
    }

    public void Init()
    {
        Debug.Log(SceneManager.GetActiveScene().name + ": <color=green>init level scripts environment</color>");

        GameObject logo3d = GameObject.FindGameObjectWithTag("level2_main_text");
        Text mainText = logo3d.GetComponent<Text>();
        mainText.text = "this text uploaded from script";
    }
}


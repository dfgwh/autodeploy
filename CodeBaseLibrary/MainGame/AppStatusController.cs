using System.Collections;
using UnityEngine;

namespace AutoDeploy.CodeBase
{
    public class AppStatusController : MonoBehaviour
    {
        public static void QuitApp()
        {
            Application.Quit();
        }
    }
}
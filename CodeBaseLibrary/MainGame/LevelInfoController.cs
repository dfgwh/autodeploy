﻿using AutoDeploy.Infrastructur;
using AutoDeploy.Logic;
using SimpleJSON;
using System;
using UnityEngine;

namespace MainGame
{
    public class LevelInfoController : MonoBehaviour, ILevelInformant
    {
        #region Properties

        public static LevelInfoController Instance { get => _instance; set => _instance = value; }
        public LevelModel CurrentLevelModel { get => _currentLevelModel; set => _currentLevelModel = value; }

        #endregion

        #region Unity Editor

        [SerializeField] private string startAddUrlPath;

        #endregion

        #region Private Fields

        private static LevelInfoController _instance = null;
        private LevelModel _currentLevelModel = null;
        private AssetBundle _currentAssetBundle = null;

        #endregion

        #region Unity Methods

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                DestroyImmediate(gameObject);
            }
        }

       // private void Start()
       // {
       //     GameBootstrapper.Instance.StartLevelInformant = this;
       // }

        #endregion

        #region Interface Methods

        public string GetLevelSceneUrl()
        {
            return CurrentLevelModel.SceneBundleURL;
        }

        public string GetLevelScriptsUrl()
        {
            return CurrentLevelModel.ScriptsBundleURL;
        }

        public void SetLevelPrefabsBundle(AssetBundle bundle)
        {
            _currentAssetBundle = bundle;
        }

        public AssetBundle GetLevelPrefabsBundle()
        {
            return _currentAssetBundle;
        }

        public void DownloadStartLevelInfo(Action<string, string> callback)
        {
            StartCoroutine(Api.GetRequest(startAddUrlPath, (code, response) =>
            {
                string scene_path = null;
                string scripts_path = null;
                if (code == 200)
                {
                    Debug.Log("get info from server: " + response);

                    JSONNode json = JSONNode.Parse(response);
                    JSONNode bundleJson = (Application.platform == RuntimePlatform.Android) ?
                        json["android"] : json["ios"];

                    CurrentLevelModel = new LevelModel(bundleJson);

                    scene_path = CurrentLevelModel.SceneBundleURL;
                    scripts_path = CurrentLevelModel.ScriptsBundleURL;
                }
                else
                {
                    Debug.LogError(code.ToString() + " " + response);
                }
                Debug.Log(scene_path);
                callback(scene_path, scripts_path);
            }));
        }

        public string GetLevelPrefabsUrl()
        {
            return CurrentLevelModel.PrefabsBundleURL;
        }

        public string GetInitScriptName()
        {
            return CurrentLevelModel.InitScriptName;
        }

        #endregion
    }
}
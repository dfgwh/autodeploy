using SimpleJSON;
using UnityEngine;

namespace MainGame
{
    public class LevelModel : MonoBehaviour
    {
        #region Properties

        public string LevelName { get => _levelName; set => _levelName = value; }
        public string SceneBundleURL { get => _sceneBundleURL; set => _sceneBundleURL = value; }
        public string ScriptsBundleURL { get => _scriptsBundleURL; set => _scriptsBundleURL = value; }
        public string PrefabsBundleURL { get => _prefabsBundleURL; set => _prefabsBundleURL = value; }
        public string InitScriptName { get => _initScriptName; set => _initScriptName = value; }

        #endregion

        #region Private Field

        private string _levelName;
        private string _initScriptName;
        private string _sceneBundleURL;
        private string _prefabsBundleURL;
        private string _scriptsBundleURL;

        #endregion

        #region Constracts

        public LevelModel(JSONNode json)
        {
            LevelName = json["level_name"];
            InitScriptName = json["init_script_name"];
            SceneBundleURL = json["scene_bundle_url"];
            ScriptsBundleURL = json["scripts_bundle_url"];
            PrefabsBundleURL = json["prefabs_bundle_url"];
        }

        #endregion
    }
}
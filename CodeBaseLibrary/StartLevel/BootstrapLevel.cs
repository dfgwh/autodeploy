using AutoDeploy.Logic;
using MainGame;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class BootstrapLevel : MonoBehaviour
{
    private StartScenesUILoader startScenesUILoader;

    private void Start()
    {
        Init();
    }

    private void Init()
    {
        Debug.Log(SceneManager.GetActiveScene().name + ": <color=green>init level scripts environment</color>");
        GameObject uiObject = GameObject.FindGameObjectWithTag("start_ui");
        if (uiObject!=null)
        {
            startScenesUILoader = uiObject.AddComponent<StartScenesUILoader>();
            AssetBundle bundle = LevelInfoController.Instance.GetLevelPrefabsBundle();
            if (bundle!=null)
            {
                GameObject levelTitleObject = bundle.LoadAsset<GameObject>("LevelTitle");
                levelTitleObject.AddComponent<LevelTitle>();
                startScenesUILoader.TitlePrefab = levelTitleObject;
                LoadData();
            }
            else
            {
                Debug.LogError("bundle is missing");
            }
            
        }
        else
        {
            Debug.LogError("start_ui tag is missing");
        }
    }

    private void LoadData()
    {
        StartCoroutine(Api.GetRequest("1--lNN5s5Amq14ty82TkYraWRSxXn2Hy3", (code, response) =>
        {
            if (code == 200)
            {
                Debug.Log("get info from server: " + response);
                startScenesUILoader.LoadUI(SimpleJSON.JSONNode.Parse(response));
            }
            else
            {
                Debug.LogError(code.ToString() + " " + response);
            }
        }));
    }
}
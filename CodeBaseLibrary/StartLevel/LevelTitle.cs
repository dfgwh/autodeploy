﻿using AutoDeploy.Logic;
using MainGame;
using UnityEngine;
using UnityEngine.UI;


public class LevelTitle : MonoBehaviour
{

    #region Unity Editor

    private Text _titleText;
    private Button _titleButton;

    #endregion

    #region Private Field

    private LevelModel _levelModel;

    #endregion

    #region Unity Methods

    private void Start()
    {
        _titleButton = GetComponentInChildren<Button>();

        _titleText = GetComponentInChildren<Text>();
    }

    #endregion

    #region Public Methods

    public void Init(LevelModel levelModel)
    {
        _levelModel = levelModel;
        SetUI();

        Debug.Log(_levelModel.LevelName);
        _titleText.text = _levelModel.LevelName;

        _titleButton.onClick.AddListener(() =>
        {
            //LevelInfoController.Instance.CurrentLevelModel = _levelModel;
            //Auto_Deploy.LoadScene(LevelInfoController.Instance.CurrentLevelModel.SceneBundleURL);
        });
    }

    #endregion

    #region Private Methods

    private void SetUI()
    {
        _titleText.text = _levelModel.LevelName;
    }

    #endregion

}

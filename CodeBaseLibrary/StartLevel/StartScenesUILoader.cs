using MainGame;
using SimpleJSON;
using UnityEngine;
using UnityEngine.UI;

public class StartScenesUILoader : MonoBehaviour
{
    #region Properties

    public GameObject TitlePrefab { set => _titlePrefab = value; }

    #endregion

    #region Private Fields

    private Transform _contentTransform;
    private GameObject _titlePrefab;

    #endregion

    #region Unity Methods

    private void Start()
    {
        _contentTransform = GetComponentInChildren<VerticalLayoutGroup>().transform;

    }

    #endregion

    #region Public Methods

    public void LoadUI(JSONNode jSONNode)
    {
        JSONArray bundleInfoArr = 
            (Application.platform == RuntimePlatform.Android) ? 
                (JSONArray)jSONNode["bundles"]["android"] : 
                (JSONArray)jSONNode["bundles"]["ios"];
        foreach (JSONNode bundle_info in bundleInfoArr)
        {
            CreateTitle(bundle_info);
        }
    }

    #endregion

    #region Private Methods

    private void CreateTitle(JSONNode bundle_info)
    {
        LevelModel levelModel = new LevelModel(bundle_info);
        GameObject levelTitleObject = Instantiate(_titlePrefab, _contentTransform) as GameObject;
        LevelTitle levelTitle = levelTitleObject.GetComponent<LevelTitle>();
        levelTitle.Init(levelModel);
    }

    #endregion
}
